# URL SHORTENER

[![pipeline status](https://gitlab.com/srustem3/url_shortener/badges/master/pipeline.svg)](https://gitlab.com/srustem3/url_shortener/-/commits/master)
[![coverage report](https://gitlab.com/srustem3/url_shortener/badges/master/coverage.svg)](https://gitlab.com/srustem3/url_shortener/-/commits/master)

This Django web-app provides short aliases for redirection of long URLs and shows redirection count of URLs that are created by authenticated users.

## Setup

This project uses different configurations for local and production environments. Both options use docker-compose approach.

### Local setup

Local setup passes current folder as a volume to docker container. It allows automatically reload Django when files change locally which makes development Django app similar to `python manage.py runserver` approach.    
Local setup doesn't use special environment variables, doesn't use nginx and gunicorn, uses SQLite as RDBMS and provides Redis container.  
`docker-compose.yaml` file is used for local setup.  

For local development use these commands:
- `docker-compose up` - run web-app and Redis (holds current terminal session and holds all logs, Ctrl+C to stop and remove containers)
- `docker-compose up -d` - run web-app and Redis in detached mode
- `docker-compose down` - stop and remove containers
- `docker-compose run --rm url_shortener_backend pytest --cov=.` - runs pytest locally in Docker container with coverage
- `docker-compose run --rm url_shortener_backend flake8` - runs flake8 lint locally in Docker container

You can also install Django app locally using Poetry and run pytest and flake8 locally. Using this approach you can even run this project without docker if you provide Redis locally.
- `poetry shell` - creates and enters to Poetry virtual environment
- `poetry install` - installs all dependencies of Django app (using `pyproject.toml` and `poetry.lock` files)
- `python manage.py migrate` - migrates database scheme to local SQLite RDBMS
- `python manage.py collectstatic` - collects static files to STATIC_ROOT dir
- `python manage.py runserver` - runs local Django server
- `cd src && pytest --cov=.` - runs pytest locally with coverage
- `cd src && flake8` - runs flake8 locally

### Production setup

Production setup uses PostgreSQL RDBMS, nginx, gunicorn and environment variables for secrets and credentials.  
`docker-compose.prod.yaml` file is used for production setup.  
This setup is used in `.gitlab-ci.yml` CI configuration. If you use it in Gitlab CI environment, you need to specify following variables using Gitlab Settings:
- `POSTGRES_DB` - any valid PostgreSQL DB name
- `POSTGRES_USER` - any valid PostgreSQL username
- `POSTGRES_PASSWORD` - any valid PostgreSQL password
- `DJANGO_SECRET_KEY` - large random value, used for securing signed data
- `DJANGO_DEBUG` - True or False
- `DJANGO_ALLOWED_HOSTS` - whitespace separated list of allowed hosts

If you run it in other environments, you need to provide these variables using `.env` file or CI variables options. You also need to provide `LATEST_IMAGE` variable (docker image tag built using Dockerfile in this folder).

Command list:
- `docker build -t ${PIPELINE_IMAGE} .` - builds docker image with web app (you need to specify PIPELINE_IMAGE environment variable)
- `docker-compose -f docker-compose.prod.yaml up` - run web-app and related services (holds current terminal session and holds all logs, Ctrl+C to stop and remove containers)
- `docker-compose -f docker-compose.prod.yaml up -d` - run web-app and related services in detached mode
- `docker-compose -f docker-compose.prod.yaml down` - stop and remove containers

## Project plan

1. Create Django project using Poetry dependency management tool (30 minutes)
1. Create and implement relational DB models architecture (3 hours)
1. Create docker-compose (include Django with Poetry, PostgreSQL, Redis) (3 hours)
1. Connect Redis and create Redis table to store mapping between original and shortened URLs (3 hours)
1. Implement main page (adding new URL) (total 4 hours)
    - Implement view and business logic of adding new URL (2 hours)
    - Implement template of main page (includes validation errors and successful adding message) (2 hours)
1. Implement redirecting using shortened URL (2 hours)
1. Implement collecting statistics about redirections (2 hours)
1. Implement registration page (total 4 hours)
    - Implement view and business logic of registering new user (2 hours)
    - Implement template of registration page (2 hours)
1. Implement authentication page (total 2 hours)
    - Implement view and business logic of authenticating new user (1 hour)
    - Implement template of authentication page (1 hours)
1. Implement list of user's URLs page (total 4 hours)
    - Implement view and business logic of list of user's URLs (2 hours)
    - Implement template of list of user's URLs page (2 hours)
1. Implement CI/CD auto-deploy to virtual server using Gitlab CI and Docker Compose (4 hours)
1. Write Readme (2 hours)

Total: 35 hours 30 minutes.  
Avg time per day: 4 hours.
