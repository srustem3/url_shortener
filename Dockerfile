FROM python:3.8.11-alpine3.14

ENV PYTHONUNBUFFERED 1
WORKDIR /app

RUN apk --no-cache --update --upgrade add build-base gcc musl-dev libffi-dev openssl-dev python3-dev postgresql-dev
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
RUN pip3 install poetry==1.1.7
ADD pyproject.toml .
ADD poetry.lock .
RUN poetry config virtualenvs.create false
RUN poetry install

COPY . .
WORKDIR /app/src
