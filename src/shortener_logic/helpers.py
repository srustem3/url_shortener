import logging
import random
import string
from typing import Tuple, List

from accounts.models import BasicUser
from django.core.cache import cache
from shortener_logic.models import UserLinkMapping


class UrlNotExistError(BaseException):
    """
    This error is raised when user tries to get not existing shortened URL in Redis
    """
    pass


def generate_random_letters(string_len: int = 10) -> str:
    """
    This function Generates random string with length `string_len` that
    contains only ASCII uppercase and lowercase letters.

    :param string_len: length of generated string
    :return: a string that contains random ASCII letters
    """
    return ''.join([random.choice(string.ascii_letters) for _ in range(string_len)])


def save_shortened_url(full_url: str, request_user: BasicUser) -> str:
    """
    This function saves shortened-full urls mapping to Redis and saves mapping between user and url
    if user is not anonymous.

    :param full_url: full url that we want to shorten
    :param request_user: request.user object of current request
    :return: generated and saved shortened path
    """

    while True:
        shortened_path = generate_random_letters()
        redis_key = f'urls:{shortened_path}'
        if cache.ttl(redis_key) == 0:
            break
    cache.set(redis_key, (full_url, 0), timeout=None)
    if not request_user.is_anonymous:
        UserLinkMapping.objects.create(shortened_path=shortened_path, owner=request_user)
    return shortened_path


def get_full_url_by_shortened(shortened_path: str) -> str:
    """
    This function gets full url by shortened path from Redis and increments redirects statistics

    :param shortened_path: randomly generated shortened path that maps to full url in Redis
    :return: full url
    """
    redis_key = f'urls:{shortened_path}'
    if cache.ttl(redis_key) == 0:
        raise UrlNotExistError()
    full_url, redirects_count = cache.get(redis_key)
    cache.set(redis_key, (full_url, redirects_count + 1), timeout=None)
    return full_url


def get_created_by_user_urls(request_user: BasicUser) -> List[Tuple[str, str, int]]:
    """
    This function returns all URLs that was created by authenticated user in format
    (short_url, full_url, redirects_count).
    If UserLinkMapping instances without Redis records are detected, UserLinkMapping instances are deleted.

    :param request_user: request.user object of current request
    :return: list of tuples in format (short_url, full_url, redirects_count).
    """

    user_short_paths = UserLinkMapping.objects.filter(owner=request_user).values_list('shortened_path', flat=True)
    user_links_with_info = []
    not_existing_user_links = []
    for short_path in user_short_paths:
        redis_key = f'urls:{short_path}'
        if cache.ttl(redis_key) != 0:
            full_url, redirects_count = cache.get(redis_key)
            user_links_with_info.append((short_path, full_url, redirects_count))
        else:
            logging.warning(f'Saved in RDBMS short path {short_path} does not exist in Redis')
            not_existing_user_links.append(short_path)
    if len(not_existing_user_links) > 0:
        UserLinkMapping.objects.filter(shortened_path__in=not_existing_user_links).delete()
    return user_links_with_info
