from django.urls import path
from shortener_logic.views import MainPageView, RedirectView, RedirectsStatistics

urlpatterns = [
    path('statistics/', RedirectsStatistics.as_view(), name='redirects_statistics'),
    path('<shortened_path>', RedirectView.as_view(), name='redirect'),
    path('', MainPageView.as_view(), name='main'),
]
