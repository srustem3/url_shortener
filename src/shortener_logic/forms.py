from django import forms
from django.core.validators import URLValidator


class NewShortenedUrl(forms.Form):
    """
    This form is used to create new shortened URLs based on full urls.
    """
    full_url = forms.CharField(
        label='Full URL',
        min_length=3,
        max_length=256,
        validators=[URLValidator()],
        required=True
    )
