from django.apps import AppConfig


class ShortenerLogicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shortener_logic'
