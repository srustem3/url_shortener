from accounts.models import BasicUser
from django.db import models


class UserLinkMapping(models.Model):
    """
    This model contains mapping between logged-in users and created by them shortened links.
    Full url and redirects count are stored in Redis, this information can be retrieved using `shortened_path` as a key
    in Redis.
    Warning! This table doesn't store links that are created by anonymous users.
    """

    shortened_path = models.CharField(max_length=10)
    owner = models.ForeignKey(BasicUser, on_delete=models.CASCADE)

    class Meta:
        indexes = [
            models.Index(fields=['owner']),
            models.Index(fields=['shortened_path']),
        ]

    def __str__(self):
        return f'{self.owner.username} - {self.shortened_path}'
