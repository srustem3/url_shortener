from pathlib import Path

from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import FormView
from shortener_logic.forms import NewShortenedUrl
from shortener_logic.helpers import save_shortened_url, get_full_url_by_shortened, UrlNotExistError, \
    get_created_by_user_urls


class MainPageView(SuccessMessageMixin, FormView):
    """
    This view is considered as main page of web-app and provides a form (GET and POST methods with validation)
    to create new shortened URLs.
    """

    template_name = Path('shortener_logic', 'main.html')
    form_class = NewShortenedUrl
    success_url = reverse_lazy('main')
    shortened_path = ''

    def get_success_message(self, cleaned_data):
        return f"Successfully created shortened URL for {cleaned_data['full_url']}:" \
               f" { self.request.scheme }://{ self.request.get_host() }/{self.shortened_path}."

    def form_valid(self, form):
        self.shortened_path = save_shortened_url(form.cleaned_data['full_url'], self.request.user)
        return super().form_valid(form)


class RedirectView(View):
    """
    This view redirects users using created shortened URLs. Returns 404 if no URLs found in Redis.
    """

    def get(self, request, shortened_path: str, *args, **kwargs):
        try:
            return redirect(get_full_url_by_shortened(shortened_path))
        except UrlNotExistError:
            raise Http404()


@method_decorator(login_required, name='dispatch')
class RedirectsStatistics(View):
    """
    This view shows all URLs that are created by current logged in user.
    """

    def get(self, request, *args, **kwargs):
        created_urls = get_created_by_user_urls(request.user)
        return render(request, Path('shortener_logic', 'redirect_statistics.html'), {'created_urls': created_urls})
