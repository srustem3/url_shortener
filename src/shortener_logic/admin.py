from django.contrib import admin
from shortener_logic.models import UserLinkMapping


admin.site.register(UserLinkMapping)
