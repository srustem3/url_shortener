import pytest
from shortener_logic.forms import NewShortenedUrl


@pytest.mark.parametrize('valid_url', [
    'https://google.com',
    'https://example.com/hello/',
    'http://example.com/',
    'http://example.com?param=value'
])
def test_valid_new_shortened_url_form_should_not_return_errors(valid_url):
    form = NewShortenedUrl({'full_url': valid_url})
    assert len(form.errors) == 0


def test_empty_new_shortened_url_form_should_fail():
    form = NewShortenedUrl({})
    assert len(form.errors) == 1
    assert form.errors.get('full_url') == ['This field is required.']


@pytest.mark.parametrize('invalid_url', ['', '1', '11111', 'something', f'https://{"a"*257}.com'])
def test_invalid_new_shortened_url_form_should_fail(invalid_url):
    form = NewShortenedUrl({'full_url': invalid_url})
    assert len(form.errors) == 1
