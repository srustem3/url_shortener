import pytest
from shortener_logic.helpers import generate_random_letters


def test_generate_random_letters_without_params_should_return_string():
    assert type(generate_random_letters()) == str


@pytest.mark.parametrize('str_len', [1, 5, 10, 20])
def test_generate_random_letters_with_params_should_return_string(str_len):
    assert type(generate_random_letters(str_len)) == str


def test_generate_random_letters_without_params_should_have_10_len():
    assert len(generate_random_letters()) == 10


@pytest.mark.parametrize('str_len', [1, 5, 10, 20])
def test_generate_random_letters_with_params_should_have_same_len(str_len):
    assert len(generate_random_letters(str_len)) == str_len


def test_generate_random_letters_without_params_should_return_only_ascii():
    assert all(map(lambda s: s.isascii(), generate_random_letters()))


@pytest.mark.parametrize('str_len', [1, 5, 10, 20])
def test_generate_random_letters_with_params_should_return_only_ascii(str_len):
    assert all(map(lambda s: s.isascii(), generate_random_letters(str_len)))
