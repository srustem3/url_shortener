import pytest
from django.urls import reverse
from shortener_logic.models import UserLinkMapping


def test_anonymous_user_should_be_redirected(client):
    response = client.get(reverse('redirects_statistics'), follow=True)
    assert len(response.redirect_chain) == 1
    assert 'login' in response.redirect_chain[0][0]
    assert response.redirect_chain[0][1] == 302


def test_logged_in_user_with_no_links_should_receive_empty_page(client, django_user_model):
    django_user_model.objects.create_user(username='user', password='password')
    client.login(username='user', password='password')
    response = client.get(reverse('redirects_statistics'), follow=True)
    assert response.status_code == 200
    assert b'You have not created any shortened URLs.' in response.content
    assert b'</table>' not in response.content


def test_logged_in_user_with_links_should_receive_them(client, django_user_model, mocker):
    django_user_model.objects.create_user(username='user', password='password')
    django_user_model.objects.create_user(username='user_2', password='password')
    client.login(username='user', password='password')
    current_user = django_user_model.objects.get(username='user')
    user_2 = django_user_model.objects.get(username='user_2')

    UserLinkMapping.objects.create(shortened_path='aAaA', owner=current_user)
    UserLinkMapping.objects.create(shortened_path='bBbB', owner=current_user)
    UserLinkMapping.objects.create(shortened_path='cCcC', owner=user_2)
    UserLinkMapping.objects.create(shortened_path='dDdD', owner=current_user)
    cache_ttl = mocker.patch('django.core.cache.cache.ttl')
    cache_ttl.side_effect = lambda key: 0 if key == 'urls:dDdD' else None
    cache_get = mocker.patch('django.core.cache.cache.get')
    cache_get.side_effect = lambda key: {
        'urls:aAaA': ('https://google.com', 1),
        'urls:bBbB': ('https://facebook.com', 2),
        'urls:cCcC': ('https://reddit.com', 2)
    }.get(key)

    response = client.get(reverse('redirects_statistics'), follow=True)
    assert response.status_code == 200
    assert b'You have not created any shortened URLs.' not in response.content
    assert b'</table>' in response.content
    assert response.content.count(b'</tr>') == 3
    assert b'aAaA' in response.content
    assert b'bBbB' in response.content
    assert b'cCcC' not in response.content
    assert b'dDdD' not in response.content


@pytest.fixture()
def detached_user_link_mapping(client, django_user_model, mocker):
    django_user_model.objects.create_user(username='user', password='password')
    client.login(username='user', password='password')
    current_user = django_user_model.objects.get(username='user')
    UserLinkMapping.objects.create(shortened_path='aAaA', owner=current_user)
    UserLinkMapping.objects.create(shortened_path='bBbB', owner=current_user)
    mocker.patch('django.core.cache.cache.ttl', return_value=0)


def test_logged_in_user_with_all_detached_links_should_receive_empty_page(client, detached_user_link_mapping):
    response = client.get(reverse('redirects_statistics'), follow=True)
    assert response.status_code == 200
    assert b'You have not created any shortened URLs.' in response.content
    assert b'</table>' not in response.content


def test_logged_in_user_with_all_detached_links_should_delete_them(client, detached_user_link_mapping):
    client.get(reverse('redirects_statistics'), follow=True)
    assert UserLinkMapping.objects.count() == 0
