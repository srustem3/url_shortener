import logging

import pytest
from django.urls import reverse

from shortener_logic.models import UserLinkMapping


@pytest.fixture()
def mock_redis_calls(mocker):
    cache_set = mocker.patch('django.core.cache.cache.set')
    cache_ttl = mocker.patch('django.core.cache.cache.ttl')
    cache_ttl.side_effect = [1, None, 0]
    return cache_set, cache_ttl


valid_urls = ['https://google.com', 'https://twitter.com', 'https://reddit.com']
invalid_urls = ['', '1', '11111', 'something']
all_urls = valid_urls + invalid_urls


def test_get_main_page_ok(client):
    response = client.get(reverse('main'))
    assert response.status_code == 200


def test_get_main_page_contains_form(client):
    response = client.get(reverse('main'))
    assert b'</form>' in response.content


def test_post_main_page_with_empty_request_should_have_validation_error_msg(client):
    response = client.post(reverse('main'), follow=True)
    assert response.status_code == 200
    assert b'error' in response.content


@pytest.mark.parametrize('full_url', valid_urls)
def test_post_main_page_valid_request_should_redirect_to_main_page(client, full_url, mock_redis_calls):
    response = client.post(reverse('main'), {'full_url': full_url}, follow=True)
    assert len(response.redirect_chain) == 1
    assert response.redirect_chain[0][0] == reverse('main')
    assert response.redirect_chain[0][1] == 302


@pytest.mark.parametrize('full_url', invalid_urls)
def test_post_main_page_with_wrong_request_should_have_validation_error_msg(client, full_url):
    response = client.post(reverse('main'), {'full_url': full_url}, follow=True)
    assert response.status_code == 200
    logging.warning(response.content)
    assert b'Enter a valid URL.' in response.content or b'This field is required.' in response.content


@pytest.mark.parametrize('full_url', valid_urls)
def test_post_main_page_with_valid_request_should_have_success_msg(client, full_url, mock_redis_calls):
    response = client.post(reverse('main'), {'full_url': full_url}, follow=True)
    messages = list(response.context['messages'])
    assert len(messages) == 1
    assert 'success' in str(messages[0]).lower()


@pytest.mark.parametrize('full_url', valid_urls)
def test_post_main_page_with_valid_request_should_call_redis_ttl(client, full_url, mock_redis_calls):
    _, cache_ttl = mock_redis_calls
    client.post(reverse('main'), {'full_url': full_url}, follow=True)
    assert cache_ttl.call_count > 0


@pytest.mark.parametrize('full_url', valid_urls)
def test_post_main_page_with_valid_request_should_call_redis_set(client, full_url, mock_redis_calls):
    cache_set, _ = mock_redis_calls
    client.post(reverse('main'), {'full_url': full_url}, follow=True)
    assert cache_set.call_count == 1


@pytest.mark.django_db()
@pytest.mark.parametrize('full_url', all_urls)
def test_post_main_page_request_with_logged_out_user_should_not_create_rdbms_record(client, full_url, mock_redis_calls):
    client.post(reverse('main'), {'full_url': full_url}, follow=True)
    assert UserLinkMapping.objects.count() == 0


@pytest.mark.django_db()
@pytest.mark.parametrize('full_url', invalid_urls)
def test_post_main_page_with_not_valid_request_with_logged_in_user_should_not_create_rdbms_record(
        client,
        full_url,
        mock_redis_calls,
        django_user_model
):
    django_user_model.objects.create_user(username='user', password='password')
    client.login(username='user', password='password')
    client.post(reverse('main'), {'full_url': full_url}, follow=True)
    assert UserLinkMapping.objects.count() == 0


@pytest.mark.django_db()
@pytest.mark.parametrize('full_url', valid_urls)
def test_post_main_page_with_valid_request_with_logged_in_user_should_create_rdbms_record(
        client,
        full_url,
        mock_redis_calls,
        django_user_model
):
    django_user_model.objects.create_user(username='user', password='password')
    client.login(username='user', password='password')
    client.post(reverse('main'), {'full_url': full_url}, follow=True)
    assert UserLinkMapping.objects.count() == 1
