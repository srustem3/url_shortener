from unittest.mock import call

import pytest
from django.urls import reverse


@pytest.fixture()
def mock_redis_with_existing_url(mocker):
    cache_ttl = mocker.patch('django.core.cache.cache.ttl', return_value=None)
    cache_get = mocker.patch('django.core.cache.cache.get', return_value=('https://google.com', 1))
    cache_set = mocker.patch('django.core.cache.cache.set')
    return cache_ttl, cache_get, cache_set


@pytest.fixture()
def mock_redis_with_not_existing_url(mocker):
    cache_ttl = mocker.patch('django.core.cache.cache.ttl', return_value=0)
    cache_get = mocker.patch('django.core.cache.cache.get', return_value=None)
    cache_set = mocker.patch('django.core.cache.cache.set')
    return cache_ttl, cache_get, cache_set


def test_redirect_with_existing_shortened_url_should_increase_redirect_count(client, mock_redis_with_existing_url):
    _, _, cache_set = mock_redis_with_existing_url
    client.get(reverse('redirect', kwargs={'shortened_path': 'random'}))
    assert cache_set.call_args == call('urls:random', ('https://google.com', 2), timeout=None)


def test_redirect_with_existing_shortened_url_should_redirect(client, mock_redis_with_existing_url):
    response = client.get(reverse('redirect', kwargs={'shortened_path': 'random'}), follow=True)
    assert len(response.redirect_chain) == 1
    assert response.redirect_chain[0][0] == 'https://google.com'
    assert response.redirect_chain[0][1] == 302


def test_redirect_with_not_existing_shortened_url_should_not_call_cache_set(client, mock_redis_with_not_existing_url):
    _, _, cache_set = mock_redis_with_not_existing_url
    client.get(reverse('redirect', kwargs={'shortened_path': 'random'}))
    assert cache_set.call_count == 0


def test_redirect_with_not_existing_shortened_url_should_return_404(client, mock_redis_with_not_existing_url):
    _, _, cache_set = mock_redis_with_not_existing_url
    response = client.get(reverse('redirect', kwargs={'shortened_path': 'random'}))
    assert response.status_code == 404
