import pytest
from accounts.forms import RegistrationForm


@pytest.mark.django_db()
@pytest.mark.parametrize('data', [
    {'email': 'example@example.com', 'username': 'user', 'password': 'password'},
    {'email': 'hi@hi.ru', 'username': 'u', 'password': 'p'},
])
def test_valid_reg_form_should_not_return_errors(data):
    form = RegistrationForm(data)
    assert len(form.errors) == 0


@pytest.mark.django_db()
def test_reg_form_without_email_should_fail():
    form = RegistrationForm({'username': 'user', 'password': 'password'})
    assert len(form.errors) == 1
    assert form.errors.get('email') == ['This field is required.']


@pytest.mark.django_db()
def test_reg_form_without_username_should_fail():
    form = RegistrationForm({'email': 'example@example.com', 'password': 'password'})
    assert len(form.errors) == 1
    assert form.errors.get('username') == ['This field is required.']


@pytest.mark.django_db()
def test_reg_form_without_password_should_fail():
    form = RegistrationForm({'email': 'example@example.com', 'username': 'user'})
    assert len(form.errors) == 1
    assert form.errors.get('password') == ['This field is required.']


@pytest.mark.django_db()
@pytest.mark.parametrize('invalid_email', ['1', 'aa', 'example.com', 'example.example.com', '@'])
def test_reg_form_with_invalid_email_should_fail(invalid_email):
    form = RegistrationForm({'email': invalid_email, 'username': 'user', 'password': 'password'})
    assert len(form.errors) == 1
    assert form.errors.get('email') == ['Enter a valid email address.']


@pytest.mark.django_db()
@pytest.mark.parametrize(
    'invalid_username, error_msg',
    [
        (['a' * 151], 'Ensure this value has at most 150 characters'),
        ('///', 'Enter a valid username')
    ]
)
def test_reg_form_with_invalid_username_should_fail(invalid_username, error_msg):
    form = RegistrationForm({'email': 'example@example.com', 'username': invalid_username, 'password': 'password'})
    assert len(form.errors) == 1
    assert error_msg in form.errors.get('username')[0]
