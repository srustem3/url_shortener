from django.urls import reverse


def test_get_login_page_for_logged_out_user_should_return_valid_page(client):
    response = client.get(reverse('login'))
    assert response.status_code == 200
    assert b'login' in response.content.lower()


def test_get_login_page_for_logged_in_user_should_redirect_to_main_page(client, django_user_model):
    django_user_model.objects.create_user(username='user', password='password')
    client.login(username='user', password='password')
    response = client.get(reverse('login'), follow=True)
    assert len(response.redirect_chain) == 1
    assert response.redirect_chain[0][0] == reverse('main')
    assert response.redirect_chain[0][1] == 302


def test_post_login_page_user_should_be_redirected_after_submitting_login_form(client, django_user_model):
    django_user_model.objects.create_user(username='user', password='password')
    response = client.post(reverse('login'), {
        'username': 'user',
        'password': 'password'
    }, follow=True)
    assert len(response.redirect_chain) == 1
    assert response.redirect_chain[0][0] == reverse('main')
    assert response.redirect_chain[0][1] == 302
