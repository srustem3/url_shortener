import pytest
from django.urls import reverse


def test_get_register_should_return_valid_page(client):
    response = client.get(reverse('register'))
    assert response.status_code == 200
    assert b'register' in response.content.lower()


@pytest.mark.django_db()
def test_post_register_valid_form_should_create_user(client):
    client.post(reverse('register'), {
        'email': 'example@example.com',
        'username': 'user',
        'password': 'password'
    }, follow=True)
    assert client.login(username='user', password='password')


@pytest.mark.django_db()
def test_post_register_valid_form_should_redirect_to_login_page(client):
    response = client.post(reverse('register'), {
        'email': 'example@example.com',
        'username': 'user',
        'password': 'password'
    }, follow=True)
    assert len(response.redirect_chain) == 1
    assert response.redirect_chain[0][0] == reverse('login')
    assert response.redirect_chain[0][1] == 302


@pytest.mark.django_db()
def test_post_register_invalid_form_should_not_create_user(client):
    client.post(reverse('register'), {
        'email': 'example.com',
        'username': 'user',
        'password': 'password'
    }, follow=True)
    assert not client.login(username='user', password='password')


@pytest.mark.django_db()
def test_post_register_invalid_form_should_return_error(client):
    response = client.post(reverse('register'), {
        'email': 'example.com',
        'username': 'user',
        'password': 'password'
    }, follow=True)
    assert response.status_code == 200
    assert b'Enter a valid email address' in response.content
