from django.contrib.auth.models import AbstractUser


class BasicUser(AbstractUser):
    """
    This model is used as a default User class.
    Default Django User model is not used to have ability to add new fields quickly.
    """
    pass
