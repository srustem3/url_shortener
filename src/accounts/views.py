from pathlib import Path

from accounts.forms import RegistrationForm
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import FormView


class RegistrationPageView(SuccessMessageMixin, FormView):
    """
    This view provides a form (GET and POST methods with validation) for new users registration.
    """

    template_name = Path('accounts', 'registration.html')
    form_class = RegistrationForm
    success_url = reverse_lazy('login')
    success_message = "You are successfully registered."

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
