from accounts.models import BasicUser
from django.forms import ModelForm, PasswordInput, CharField, EmailField


class RegistrationForm(ModelForm):
    """
    This form provides ability to register new users.
    """

    email = EmailField(required=True)
    password = CharField(widget=PasswordInput())

    class Meta:
        model = BasicUser
        fields = ('email', 'username', 'password')

    def save(self, **kwargs):
        new_user = super().save(**kwargs)
        new_user.set_password(self.cleaned_data['password'])
        new_user.save()
        return new_user
