from pathlib import Path

from accounts.views import RegistrationPageView
from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('login/', LoginView.as_view(
        template_name=Path('accounts', 'login.html'),
        redirect_authenticated_user=True
    ), name='login'),
    path('logout/', LogoutView.as_view(template_name=Path('accounts', 'logout.html'),), name='logout'),
    path('register/', RegistrationPageView.as_view(), name='register')
]
